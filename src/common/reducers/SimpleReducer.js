import { assign } from 'helpers/dataTypesHelpers/ObjectHelper';
import { ACTIONS } from 'common/Constants';

const { SIMPLE_TEXT } = ACTIONS;

const updateText = ({ text }, state, store) => assign({}, state, { text });

export default (state, action, store) => {

	switch (action.type) {
		case SIMPLE_TEXT.UPDATE:
			return updateText(action.data, state, store);

		default:
			return state || {};
	};
};