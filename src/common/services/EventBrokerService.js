import SubscriptionBaseClass from 'mixins/SubscriptionBaseClass';
import Utils from 'helpers/Utils';

class EventBrokerService extends SubscriptionBaseClass {
	constructor(options) {
		super(options);
		return this;
	}

	throttle(scope = window, originalEvtContext = window, originalEvtType, callback) {
		if (!originalEvtType) {
			return this;
		};

		let running = false;

		this.addProxyEvent(scope, originalEvtContext, originalEvtType, callback, (evt) => {

			if (running) {
				return;
			};

			running = true;

			requestAnimationFrame(() => {
				this.emitChange(originalEvtType, () => evt);
				running = false;
			});
		});

		return this;
	}

	removeThrottle(scope, originalEvtContext, originalEvtType) {
		this.removeProxyEvent(scope, originalEvtContext, originalEvtType);
		return this;
	}

	addProxyEvent(scope, originalEvtContext = window, originalEvtType, callback, proxyCb = null) {
		if (!originalEvtType) {
			return this;
		};

		const proxy = proxyCb || ((evt) => this.emitChange(originalEvtType, () => evt));
		this.subscribe(scope, originalEvtType, callback, proxy);

		originalEvtContext.addEventListener(originalEvtType, proxy, false);
		return this;
	}

	removeProxyEvent(scope = {}, originalEvtContext = window, originalEvtType) {
		if (!originalEvtType) {
			return this;
		};
		
		const storedEvent = this.getStoredEventByType(originalEvtType);

		if (!storedEvent) {
			return this;
		};

		this.unsubscribe(scope, originalEvtType);
		originalEvtContext.removeEventListener(originalEvtType, storedEvent.proxy);
		return this;
	}
}

export default new EventBrokerService();