
/**
 * Returns true if param is a String.
 * 
 * @param {string} string, param to be evaluated
 */
const isString = (string) => typeof string === 'string';

/**
 * Converts a given string to a camel case version.
 * 
 * @param {string} string to be transformed, eg: ADD_USER
 * @param {string} characterDivider divider, eg: '_'
 * @param {boolean} ignoreFirstWord if true, the first word will be capitalized
 * @returns {string} new string, eg: addUser
 */
const toCamelCase = (string, characterDivider, ignoreFirstWord) => {
	return (string.split(characterDivider).map((word, i) => {
		return (!ignoreFirstWord && i === 0) && word.toLowerCase() || 
			(word[0].toLocaleUpperCase() + word.slice(1).toLowerCase());
	})).join('');
}; 


export default {
	toCamelCase,
	isString
};