import functionHelper from './FunctionHelper';

/**
 * Check if element is already in the array.
 * 
 * @param {Arry} array to be analyzed
 * @param {Function} comparer
 * @returns {Boolean}
 */
export const inArray = (array, comparer) => {
	const length = array.length;
	let index    = 0;

	for(index; index < length; index++) { 
		if (functionHelper.execFunction(comparer, array[index]) ) return true; 
	}
	return false; 
}; 

/**
 * Adds an element to array if is not in it already.
 * 
 * @param {Arry} array to be analyzed
 * @param {any} element
 * @param {Function} comparer
 * @returns {Arry}
 */
export const pushIfNotExist = (array =[], element, comparer = (item) => (array.indexOf(element) > -1) ) => { 
	let tempArry = [];

	if (!inArray(array, comparer)) tempArry.push(element);
	return array.concat(tempArry);
};

/**
 * Converts arguments (is an Array-like) into an array.
 * 
 * @param {arguments}
 * @returns {Arry}
 */
export const argumentsToArray =  (...args) => { 
	return Array.apply(null, args);
};

/**
 * Converts arguments (is an Array-like) into an array, and merge both.
 * 
 * @param {Arry} array
 * @param {arguments} args
 * @returns
 */
export const pushToArguments = function (array, args){ 
	return array.concat(argumentsToArray.apply(null, args));
};

export const groupBy = (array, prop) => {
	return array && array.reduce((groups, item) => {
		const val = item[prop];
		groups[val] = groups[val] || [];
		groups[val].push(item);
		return groups;
	}, {}) || {};
};

export default{
	inArray,
	pushIfNotExist,
	argumentsToArray,
	pushToArguments,
	groupBy
};