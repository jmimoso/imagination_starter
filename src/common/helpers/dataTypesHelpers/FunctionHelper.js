import arrayHelper from './ArrayHelper';


	
/**
 * Check if is a Function
 * 
 * @static
 * @param {function} fn 
 * @returns {boolean}
 */
export const isFunction = (fn) => {
	return typeof fn == 'function' || false;
}

/**
 * Calls the given function and pass the parameters
 * 
 * @param {function} fn 
 * @param {any} params 
 * @returns {any}
 */
export const execFunction = (fn, ...params) => {
	if(fn && isFunction(fn)){ 
		return fn.apply(null, params);
	};
}
export default {
	isFunction,
	execFunction
}