import Log from 'handlers/DebugHandler';
import SubscriptionBaseClass from 'mixins/SubscriptionBaseClass';

class Store extends SubscriptionBaseClass{
	constructor(props) {
		super(props);

		this._subscribersStorageKey = 'subscribe';
		
		if(process.env.NODE_ENV !== 'production') {
			this.buildInitState = this.buildInitState || (() => { 
				throw new Error('This method must be implemented, returns the data that will be stored'); })
			
			this.persistenceProvider = this.persistenceProvider || (() => { 
				throw new Error('This method must be implemented, returns api service'); })
		};

		return this;
	}

	storeData(newData) {
		this._data = newData || this.buildInitState();
		this.emitChange(this._subscribersStorageKey, this.storedData);
		return this;
	}

	get(key) {
		const value = this.storedData()[key];
		return value !== undefined ? value : this.buildInitState()[key];
	}
	
	bind(scope, callback) {
		this.subscribe(scope, this._subscribersStorageKey, callback);
		return this;
	}

	unbind(scope) {
		this.unsubscribe(scope, this._subscribersStorageKey);
		return this;
	}

	storedData() {
		return this._data || this.buildInitState();
	}
};

export default Store;