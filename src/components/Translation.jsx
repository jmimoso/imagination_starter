import React, { Component } from 'react';

import TranslationsHandler from 'handlers/TranslationsHandler';
import Utils from 'helpers/Utils';

class Translation extends Component {
	constructor(props) {
		super(props);

		this._uuid = Utils.generateUUID();
		this.state = this.buildState();
		return this;
	}

	componentDidMount() {
		TranslationsHandler.bind(this, this.handleChange);
		return this;
	}
	
	componentWillUnmount() {
		TranslationsHandler.unbind(this);
		return this;
	}

	buildState() {
		const { tkey, comp } = this.props;
		const translation = (tkey !== undefined && TranslationsHandler.translate(tkey)) || tkey;
		const TranslationComp = (comp !== undefined && TranslationsHandler.translate(comp)) || tkey;

		return {
			translation,
			TranslationComp
		};
	}

	handleChange() {
		const newState = this.buildState();
		const { translation } = newState;

		if (translation && translation === this.state.translation) {
			return this;
		};

		this.setState(newState);
		return this;
	}

	render() {
		const { props } = this;
		const { translation, TranslationComp } = this.state;
		const className = props.className || '';
		const children = [].concat(props.children).join('');
		
		if (translation) {
			return translation.concat(children);
		};

		return <TranslationComp className='text translation' />
	}
};

export default Translation;