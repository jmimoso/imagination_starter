import React, { Component } from 'react';
import ReactSVG from 'react-svg';
import Slider from 'react-slick';

import Translation from 'components/Translation';


export default class BlockView extends Component {


	constructor(props) {
		super(props);
		this.state = {
			firstRoll: props.firstRoll,
			isRolling: props.isRolling
		};
		return this;
	}

	componentWillReceiveProps(nextProps) {
		this.setState({
			firstRoll: nextProps.firstRoll
		});
		if(nextProps.isRolling) {
			this.goToSlider();
		}
	}

	render() {
		let sliderSetting = {
			autoplay               : false,
			autoplaySpeed          : 500,
			accessibility          : true,
			dots                   : false,
			arrows                 : false,
			draggable              : false,
			pauseOnHover           : false,
			swipe                  : false,
			touchMove              : false,
			vertical               : true,
			infinite               : true,
			speed                  : 200,
			slidesToShow           : 1,
			slidesToScroll         : 1
		};
		return (
			<li className={"block-container" + (this.state.firstRoll ? ' hideSlide' : '')} >
				<ReactSVG
					path={require('images/' + this.props.block.name + '.svg')}
					className='name'
					wrapperClassName='name-wrapper'>
				</ReactSVG>
				<div className='slot-container'>
					<div className='slot-wrapper'>
						<div className='slot' style={this.slotStyle()}>
							<Slider ref={slider => this.slider = slider} {...sliderSetting} slickGoTo={this.goToSlider}>
								{this._renderSliderImages()} 
							</Slider>
						</div>
					</div>
				</div>
			</li>
		);
	}

	_renderSliderImages() {
		return this.props.block.images.map((img, key) => {
			return (
				<div key={key} className='slide-image'>
					<img src={require('images/' + img + '.png')} />
				</div>
			);
		});
	}

	slotStyle() {
		if(this.state.firstRoll) {
			return {
				backgroundImage: `url(${require('images/starter.png')})`
			}
		} else {
			return {
				backgroundImage: ''
			}
		}
	}

	randomNumber() {
		return (1 + Math.floor(Math.random() * this.props.block.images.length));
	}

	goToSlider = (ev) => {
		this.slider.slickGoTo(this.randomNumber());
	}
	
	
}