import React, { Component } from 'react';
import ReactSVG from 'react-svg';
import Utils from 'helpers/Utils';

import BlockView from './BlockView';


class EntryController extends Component {


	constructor(props) {
		super(props);
		this.state = {
			firstRoll: true,
			isRolling: false
		};
		return this;
	}

	componentWillMount() {
		this.fetchJson();
	}

	componentDidMount() {
		return this;
	}

	componentWillUnmount() {
		return this;
	}

	render() {
		return (
			<div className="entry-container">
				<div className="entry-wrapper">
					<div className="title-container">
						<ReactSVG
							path={require('images/title.svg')}
							className='title'
							wrapperClassName='title-wrapper'>
						</ReactSVG>
					</div>
					<div className="content-container">
						<ul className="content-wrapper">
							{this.state.data ? this._renderBlock() : null}
							<div className='button-container'>
								<div className='button-wrapper'>
									<button className='button' onClick={this.rollImages}>
										<ReactSVG
											path={require('images/start.svg')}
											className='button-image'>
										</ReactSVG>
									</button>
								</div>
							</div>
						</ul>
					</div>
				</div>
			</div>
		);
	}

	_renderBlock() {
		return this.state.data.map((block, key) => {
			return (
				<BlockView block={block} key={key} isRolling={this.state.isRolling} firstRoll={this.state.firstRoll} />
			);
		});
	}

	fetchJson = () => {
		return fetch('assets/json/options.json')
        .then(response => {
        	return response.json()
        })
        .then(data => {
        	this.setState({
        		data: data.data
        	});
        })
        .catch(err => {
        	console.log('Error Fetching: ', err);
        });
	}

	rollImages = (ev) => {
		if(this.state.firstRoll) {
			this.setState({
				firstRoll: false
			});
			setTimeout(() => {
				if(!this.state.isRolling) {
					this.setState({
						isRolling: true
					}, () => {
						setTimeout(() => {
							this.setState({
								isRolling: false
							});
						}, 200);
					});
				} else {
					ev.preventDefault();
				}
			}, 200);
		} else {
			if(!this.state.isRolling) {
				this.setState({
					isRolling: true
				}, () => {
					setTimeout(() => {
						this.setState({
							isRolling: false
						});
					}, 200);
				});
			} else {
				ev.preventDefault();
			}
		}
		
	}


};

export default EntryController;