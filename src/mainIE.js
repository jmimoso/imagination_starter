import BabelPolyfill from 'babel-polyfill';
import Raf from 'raf/polyfill';

export default () => ({
	BabelPolyfill,
	Raf
});