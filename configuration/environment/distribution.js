const { join } = require('path');
const { createIndexJS } = require('ui-building-helper/lib/generateAssets');
const { UIModules } = require('ui-building-helper');
const webpack = require('webpack');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const GenerateAssetPlugin = require('generate-asset-webpack-plugin');

module.exports = function (env, basePath) {
	const name = UIModules.getName();
	const utilities = join(UIModules.getProjectDirPath('src_dir'), 'sass/utilities/');
	const dist_dir = UIModules.getProjectDirPath('dist_dir');

	return {
		target: 'node',

		output: {
			path: dist_dir,
			filename: ''.concat('js/', name, '.js'),
			library: name,
			libraryTarget: 'umd',
			publicPath: ''
		},
		
		module: {
			rules: [
				{
					test: /\.(gif|png|jpeg|jpg|svg)?$/i,
					use: [
						'file-loader',
						{
							loader: 'image-webpack-loader',
							options: {
								mozjpeg: {
									progressive: true,
									quality: 65
								},
								optipng: {
									quality: 65
								},
								pngquant: {
									quality: '65-90',
									speed: 4
								},
								gifsicle: {
									interlaced: false,
								},
								webp: {
									quality: 75
								}
							}
						}
					]
				}
			]
		},

		plugins: [
			new CleanWebpackPlugin([UIModules.getManifestConfigsProp('dist_dir')], { 'root': basePath }),

			new webpack.BannerPlugin('version: ' + UIModules.getManifest().version),

			new CopyWebpackPlugin([{
				context: utilities,
				from: '*',
				to: join(dist_dir, 'sass')
			}]),

			new GenerateAssetPlugin({
				filename: '../index.js',
				fn: (compilation, cb) => cb(null, createIndexJS(compilation, UIModules.getManifest())),
			}),

			new webpack.IgnorePlugin(/vertx/),
		]
	}
};