module.exports = (env) => { 
	return {
		target : "web",
		devtool: 'source-map',

		module: {
			rules: [
				{
					test: /\.(gif|png|jpeg|jpg|svg)?$/i,
					use: ['file-loader?name=assets/images/[name].[ext]&limit=10000']
				}
			]
		}
	}
};