const { join } = require('path');
const { UIModules } = require('ui-building-helper');
const webpack = require('webpack');

module.exports = function (env, basePath) {
	const name = UIModules.getName();

	return {
		target: "web",
		devtool: false,

		output: {
			path: UIModules.getProjectDirPath('public_dir'),
			filename: 'js/[name].bundle.js',
			library: name,
			libraryTarget: 'umd',
			publicPath: ''
		},
		
		module: {
			rules: [
				{
					test: /\.(gif|png|jpeg|jpg|svg)?$/i,
					use: [
						'file-loader?name=assets/images/[name].[ext]&limit=10000',
						{
							loader: 'image-webpack-loader?name=assets/images/[name].[ext]',
							options: {
								mozjpeg: {
									progressive: true,
									quality: 65
								},
								optipng: {
									quality: 65
								},
								pngquant: {
									quality: '65-90',
									speed: 4
								},
								gifsicle: {
									interlaced: false,
								},
								webp: {
									quality: 75
								}
							}
						}
					]
				}
			]
		},

		plugins: [
			new webpack.BannerPlugin("version: " + UIModules.getManifest().version)
		]
	}
};